---
title: Parkour Spot Finder
author: theSuess
description: Find the spots for tomorrow - Today
link: 'https://play.google.com/store/apps/details?id=com.trumpstuff.spotfinder'
---

### Privacy Policy

By submitting a Spot you agree, that every information entered, will be
published _as-is_.
Parkour Spot Finder does _not_ collect any user data whatsoever. The requested
user permissions are ineffective and were used for integration with the parse
platform. This Platform is _shut down_ and therefore not used anymore.
