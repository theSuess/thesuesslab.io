---
title: 'Openshift quick and dirty'
description: All you need to try out Openshift
author: theSuess
---

## Prerequisites

* Docker
* RHEL CentOS Fedora


## Installation

### Preparing the firewall

You need to open the following ports on the firewall:

* `8443` for the Openshift Console
* `80`,`443` for the router

You can acomplish this by doing the following:

```{.bash}
firewall-cmd --zone=public --add-port=8443/tcp --permanent
firewall-cmd --zone=public --add-port=80/tcp --permanent
firewall-cmd --zone=public --add-port=443/tcp --permanent
firewall-cmd --reload
```

### Starting the container

Since we just want to get something running, we'll mount the whole data directory

The first start will generate the neccesary configuration files


```{.bash}
docker run -d --name "origin-temp" \
  --privileged --pid=host --net=host \
  -v /:/rootfs:ro -v /var/run:/var/run:rw -v /sys:/sys -v /var/lib/docker:/var/lib/docker:rw \
  -v /var/lib/origin/:/var/lib/origin/ \
  openshift/origin:v1.3.0-alpha.2 start --write-config=/var/lib/origin/openshift.local.config/
```

When the container stops, delete it with `docker rm origin-temp`

Now we'll launch the master container

```{.bash}
docker run -d --name "origin" \
  --privileged --pid=host --net=host \
  -v /:/rootfs:ro -v /var/run:/var/run:rw -v /sys:/sys -v /var/lib/docker:/var/lib/docker:rw \
  -v /var/lib/origin:/var/lib/origin \
  openshift/origin:v1.3.0-alpha.2 start \
  --master-config=/var/lib/origin/openshift.local.config/master/master-config.yaml \
  --node-config=/var/lib/origin/openshift.local.config/node-vviecore14/node-config.yaml
```

The openshift client tools are preinstalled and configured so we enter the container

```{.bash}
docker exec -it origin bash
```



### Setting up the docker registry

Openshift needs an internal docker registry to generate custom builds.

```{.bash}

oc create serviceaccount registry -n default # Create the service account

oadm policy add-scc-to-user privileged system:serviceaccount:default:registry # Grant permissions

oadm registry --service-account=registry \ # Launch the registry
  --config=/var/lib/origin/openshift.local.config/master/admin.kubeconfig \
  --mount-host=/var/lib/origin/docker-registry
```

The id of the user under which the registry runs is 1001 so we need to change the ownership of the folder

```{.bash}
chown -R 1001:root /var/lib/origin/docker-registry
```


If everything worked correctly, `oc status` should return something like this:

    svc/docker-registry - <ip>:5000
      dc/docker-registry deploys docker.io/openshift/origin-docker-registry:v1.2.0
        deployment #1 deployed 3 hours ago - 1 pod
                       ^
                    this is the important value

Since setting up SSL is not important for a test environment we'll skip it and add our
registry as insecure. You can do this either by modifying dockers configuration files
or editing the docker service directly.

    ...
    ExecStart=/usr/bin/docker --insecure-registry <ip>:5000 daemon -H fd://
    ...

Now you just need to reload systemd and restart docker and your own docker
repository is up and running!


### Setting up the Router

The router is responsible for connecting the openshift applications to the outer network

Setting up a router is way more easy than the registry.

```{.bash}
oadm router --replicas=1 \
  --service-account=router
```

To use subdomains you need a wildcard-record like so:

    *.cloudapps.example.com. 300 IN  A 192.168.133.2

If you do not own a domain you can give [xip.io](xip.io) a try.


Statistics about the router can be accessed via

    <ip>:1936

The password can be found in the router container. Attach yourself to it (`oc rsh`)
and look for `admin` in the `haproxy.conf`


### Loading the Image-Streams

The templates are located in the `openshift-ansible` repository. To clone it you just need to execute

```{.bash}
git clone https://github.com/openshift/openshift-ansible
```

Depending on if you want to use rhel or centos you can import the image streams with

```{.bash}
oc create -f openshift-ansible/roles/openshift_example/files/examples/latest/image-streams/image-stream-rhel7.json -n openshift
```

or

```{.bash}
oc create -f openshift-ansible/roles/openshift_example/files/examples/latest/image-streams/image-stream-centos7.json -n openshift
```

danach benötigt openshift noch die Datenbank-Templates und Quickstart-Templates
Import the templates with:

```{.bash}
oc create -f openshift-ansible/roles/openshift_example/files/examples/latest/db-templates \ 
    -n openshift
oc create -f openshift-ansible/roles/openshift_example/files/examples/latest/quickstart-templates \
    -n openshift
```

### User Authentication

If you want to configure user authentication, please refer to the following article
[https://docs.openshift.org/latest/install_config/configuring_authentication.html](https://docs.openshift.org/latest/install_config/configuring_authentication.html)

## Custom Templates

You can make a template out of every docker container. To try this
simply read a few of the templates which we imported earlier.

To run a container as root you need the following policy

    oadm policy add-scc-to-user anyuid -z default
<!--
--[ 4 - Inter-Container-Communication

Durch das linken von containern werden lediglich DNS einträge injected.

--[ 5 - Storage

Openshifts Storage funktioniert wie folgt:

          /---------------------------------------\~~~\
          | Container                             |   |
          |        /------------------------------|   } Virtual Layer
          |        | PersistantVolumeClaim        |   |
          |--------|------------------------------|~~~\
          | docker | PersistantVolume             |   |
          |        |  | - hostPath                |   |
          |        |  | - NFS                     |   } Host/Hypervisor/Network Layer
          |        |  | - GlusterFS               |   |
          |        \  \ - ...                     |   |
          |         \-----------------------------|   |
          \---------------------------------------/~~~/

Um also Storage bereitzustellen muss zuerst ein Persistant volume erstellt werden.


  apiVersion: v1
  kind: PersistentVolume
  metadata:
    name: stor01
  spec:
    capacity:
      storage: 512M
    accessModes:
      - ReadWriteOnce
    persistentVolumeReclaimPolicy: Recycle
    hostPath:
      path: /tmp/zeugs

Danach kann ein Claim für eine DC automatisch bereitgestellt werden

    oc volume dc/nginx --add --claim-size 5M --mount-path /usr/share/nginx/html --name html

Überprüft kann das ganze so werden:

    $ oc get pvc
    NAME        STATUS    VOLUME    CAPACITY   ACCESSMODES   AGE
    pvc-byk0p   Bound     stor01    512M       RWO           1h

    $ oc get pv
    stor01    512M       RWO           Bound     automic/pvc-byk0p             1h

    $ oc volume dc --all
    deploymentconfigs/nginx
      pvc/pvc-byk0p (allocated 512MB) as html
        mounted at /usr/share/nginx/html

Andere container können ebenso auf den selben VolumeClaim Zugreifen
-->
