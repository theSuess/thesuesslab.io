---
title: Contact
---

If you have some questions or other concerns, contact me at
[thesuess@teknik.io](mailto:thesuess@teknik.io)
