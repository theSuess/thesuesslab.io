--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}

import Data.Monoid (mappend)
import Hakyll

--------------------------------------------------------------------------------
config :: Configuration
config = defaultConfiguration {destinationDirectory = "public"}

main :: IO ()
main =
  hakyllWith config $
  do match "images/*" $
       do route idRoute
          compile copyFileCompiler
     match "css/*" $
       do route idRoute
          compile compressCssCompiler
     match (fromList ["about.md","contact.md","projects.html"]) $
       do route $ setExtension "html"
          compile $
            pandocCompiler >>=
            loadAndApplyTemplate "templates/default.html" defaultContext >>=
            relativizeUrls
     match "posts/*" $
       do route $ setExtension "html"
          compile $
            pandocCompiler >>=
            loadAndApplyTemplate "templates/post.html" postCtx >>=
            loadAndApplyTemplate "templates/default.html" postCtx >>=
            relativizeUrls
     match "projects/*" $
       do route $ setExtension "html"
          compile $
            pandocCompiler >>=
            loadAndApplyTemplate "templates/project.html" defaultContext >>=
            loadAndApplyTemplate "templates/default.html" defaultContext >>=
            relativizeUrls
     create ["blog.html"] $
       do route idRoute
          compile $
            do posts <- recentFirst =<< loadAll "posts/*"
               let archiveCtx =
                     listField "posts" postCtx (return posts) `mappend`
                     constField "title" "Blog" `mappend`
                     defaultContext
               makeItem "" >>=
                 loadAndApplyTemplate "templates/blog.html" archiveCtx >>=
                 loadAndApplyTemplate "templates/default.html" archiveCtx >>=
                 relativizeUrls
     create ["projects.html"] $
       do route idRoute
          compile $
            do projects <- loadAll "projects/*"
               let projectsCtx =
                     listField "projects" projectsCtx (return projects) `mappend`
                     defaultContext
                   titleCtx =
                     constField "title" "Projects" `mappend` defaultContext
               makeItem "" >>=
                 loadAndApplyTemplate "templates/projects.html" projectsCtx >>=
                 loadAndApplyTemplate "templates/default.html" titleCtx >>=
                 relativizeUrls
     match "index.html" $
       do compile pandocCompiler
          route idRoute
          compile $
            do posts <- fmap (take 3) . recentFirst =<< loadAll "posts/*"
               let indexCtx =
                     listField "posts" postCtx (return posts) `mappend`
                     constField "title" "Hello, I'm Dominik" `mappend`
                     defaultContext
               getResourceBody >>= applyAsTemplate indexCtx >>=
                 loadAndApplyTemplate "templates/default.html" indexCtx >>=
                 relativizeUrls
     match "templates/*" $ compile templateBodyCompiler

--------------------------------------------------------------------------------
postCtx :: Context String
postCtx = dateField "date" "%B %e, %Y" `mappend` defaultContext
